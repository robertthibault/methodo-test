package exercices;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CrousTU {
	
	private Crous crous;
	private Student student;
	
	@BeforeEach
	public void setUp() {
		crous = new Crous();
		student = new Student();
		student.setId("1");
		student.setFirstname("Thibault");
		student.setLastname("ROBERT");
	}
	
	@Test
	public void testGiveThirdRightFalse() {
		student.setNbCredit(60);
		student.setNbYear(3);
		student.setNbSemester(2);
		student.setNbGrant(5);
		assertFalse(crous.additionalGrant(student));
	}
	
	@Test
	public void testGiveThirdRightTrue() {
		student.setNbCredit(30);
		student.setNbYear(1);
		student.setNbSemester(1);
		student.setNbGrant(2);
		assertTrue(crous.additionalGrant(student));
		
		student.setNbCredit(60);
		student.setNbYear(0);
		student.setNbSemester(0);
		student.setNbGrant(2);
		assertTrue(crous.additionalGrant(student));
		
		student.setNbCredit(0);
		student.setNbYear(0);
		student.setNbSemester(2);
		student.setNbGrant(2);
		assertTrue(crous.additionalGrant(student));
		
		student.setNbCredit(0);
		student.setNbYear(0);
		student.setNbSemester(0);
		student.setNbGrant(2);
		student.setProblem(true);
		assertTrue(crous.additionalGrant(student));
		student.setProblem(false);
		student.setStudies(true);
		assertTrue(crous.additionalGrant(student));
		student.setStudies(false);
		student.setIntership(true);
		assertTrue(crous.additionalGrant(student));
		student.setIntership(false);
		student.sethOrS(true);
		assertTrue(crous.additionalGrant(student));
	}
	
	@Test
	public void testGiveFourthRightFalse() {
		student.setNbCredit(30);
		student.setNbYear(1);
		student.setNbSemester(1);
		student.setNbGrant(3);
		assertFalse(crous.additionalGrant(student));
		
		student.setNbCredit(60);
		student.setNbYear(0);
		student.setNbSemester(0);
		student.setNbGrant(3);
		assertFalse(crous.additionalGrant(student));
		
		student.setNbCredit(0);
		student.setNbYear(0);
		student.setNbSemester(2);
		student.setNbGrant(3);
		assertFalse(crous.additionalGrant(student));
		
		student.setNbCredit(0);
		student.setNbYear(0);
		student.setNbSemester(0);
		student.setUsed(true);
		student.setProblem(true);
		assertFalse(crous.additionalGrant(student));
		student.setProblem(false);
		student.setStudies(true);
		assertFalse(crous.additionalGrant(student));
		student.setStudies(false);
		student.setIntership(true);
		assertFalse(crous.additionalGrant(student));
		student.setIntership(false);
		student.sethOrS(true);
		assertFalse(crous.additionalGrant(student));
	}
	
	@Test
	public void testGiveFourthRightTrue() {
		student.setNbCredit(120);
		student.setNbYear(0);
		student.setNbSemester(0);
		student.setNbGrant(3);
		assertTrue(crous.additionalGrant(student));
		
		student.setNbCredit(60);
		student.setNbYear(2);
		student.setNbSemester(0);
		student.setNbGrant(3);
		assertTrue(crous.additionalGrant(student));
		
		student.setNbCredit(0);
		student.setNbYear(0);
		student.setNbSemester(4);
		student.setNbGrant(3);
		assertTrue(crous.additionalGrant(student));
		
		student.setNbCredit(0);
		student.setNbYear(0);
		student.setNbSemester(0);
		student.setProblem(true);
		assertTrue(crous.additionalGrant(student));
		student.setProblem(false);
		student.setStudies(true);
		assertTrue(crous.additionalGrant(student));
		student.setStudies(false);
		student.setIntership(true);
		assertTrue(crous.additionalGrant(student));
		student.setIntership(false);
		student.sethOrS(true);
		assertTrue(crous.additionalGrant(student));
	}
	
	@Test
	public void testGiveFifthRightFalse() {
		student.setNbCredit(30);
		student.setNbYear(1);
		student.setNbSemester(1);
		student.setNbGrant(4);
		assertFalse(crous.additionalGrant(student));
		
		student.setNbCredit(60);
		student.setNbYear(0);
		student.setNbSemester(0);
		student.setNbGrant(4);
		assertFalse(crous.additionalGrant(student));
		
		student.setNbCredit(0);
		student.setNbYear(0);
		student.setNbSemester(2);
		student.setNbGrant(4);
		assertFalse(crous.additionalGrant(student));
		
		student.setNbCredit(0);
		student.setNbYear(0);
		student.setNbSemester(0);
		student.setUsed(true);
		student.setProblem(true);
		assertFalse(crous.additionalGrant(student));
		student.setProblem(false);
		student.setStudies(true);
		assertFalse(crous.additionalGrant(student));
		student.setStudies(false);
		student.setIntership(true);
		assertFalse(crous.additionalGrant(student));
		student.setIntership(false);
		student.sethOrS(true);
		assertFalse(crous.additionalGrant(student));
	}
	
	@Test
	public void testGiveFifthRightTrue() {
		student.setNbCredit(120);
		student.setNbYear(0);
		student.setNbSemester(0);
		student.setNbGrant(4);
		assertTrue(crous.additionalGrant(student));
		
		student.setNbCredit(60);
		student.setNbYear(2);
		student.setNbSemester(0);
		student.setNbGrant(4);
		assertTrue(crous.additionalGrant(student));
		
		student.setNbCredit(0);
		student.setNbYear(0);
		student.setNbSemester(4);
		student.setNbGrant(4);
		assertTrue(crous.additionalGrant(student));
		
		student.setNbCredit(0);
		student.setNbYear(0);
		student.setNbSemester(0);
		student.setProblem(true);
		assertTrue(crous.additionalGrant(student));
		student.setProblem(false);
		student.setStudies(true);
		assertTrue(crous.additionalGrant(student));
		student.setStudies(false);
		student.setIntership(true);
		assertTrue(crous.additionalGrant(student));
		student.setIntership(false);
		student.sethOrS(true);
		assertTrue(crous.additionalGrant(student));
	}
	
	@Test
	public void testGiveSixthRightFalse() {
		
		student.setNbCredit(120);
		student.setNbYear(0);
		student.setNbSemester(0);
		student.setNbGrant(5);
		assertFalse(crous.additionalGrant(student));
		
		student.setNbCredit(60);
		student.setNbYear(2);
		student.setNbSemester(0);
		student.setNbGrant(5);
		assertFalse(crous.additionalGrant(student));
		
		student.setNbCredit(0);
		student.setNbYear(0);
		student.setNbSemester(4);
		student.setNbGrant(5);
		assertFalse(crous.additionalGrant(student));
		
		student.setNbCredit(120);
		student.setNbYear(0);
		student.setNbSemester(0);
		student.setUsed(true);
		student.setProblem(true);
		assertFalse(crous.additionalGrant(student));
		student.setProblem(false);
		student.setStudies(true);
		assertFalse(crous.additionalGrant(student));
		student.setStudies(false);
		student.setIntership(true);
		assertFalse(crous.additionalGrant(student));
		student.setIntership(false);
		student.sethOrS(true);
		assertFalse(crous.additionalGrant(student));
	}
	
	@Test
	public void testGiveSixthRightTrue() {
		
		student.setNbYear(4);
		assertTrue(crous.additionalGrant(student));
		student.setNbYear(5);
		assertTrue(crous.additionalGrant(student));
		
		student.setNbCredit(120);
		student.setNbYear(0);
		student.setNbSemester(0);
		student.setNbGrant(5);
		student.setProblem(true);
		assertTrue(crous.additionalGrant(student));
		student.setUsed(true);
		assertFalse(crous.additionalGrant(student));
		student.setUsed(false);
		student.setProblem(false);
		student.setStudies(true);
		assertTrue(crous.additionalGrant(student));
		student.setUsed(true);
		assertFalse(crous.additionalGrant(student));
		student.setUsed(false);
		student.setStudies(false);
		student.setIntership(true);
		assertTrue(crous.additionalGrant(student));
		student.setUsed(true);
		assertFalse(crous.additionalGrant(student));
		student.setUsed(false);
		student.setIntership(false);
		student.sethOrS(true);
		assertTrue(crous.additionalGrant(student));
		student.setUsed(true);
		assertFalse(crous.additionalGrant(student));
	}
	
	@Test
	public void testGiveMoreOrEqualsSeventhRightFalse() {
		
		student.setNbCredit(120);
		student.setNbYear(0);
		student.setNbSemester(0);
		student.setNbGrant(6);
		assertFalse(crous.additionalGrant(student));
		
		student.setNbCredit(60);
		student.setNbYear(2);
		student.setNbSemester(0);
		student.setNbGrant(6);
		assertFalse(crous.additionalGrant(student));
		
		student.setNbCredit(0);
		student.setNbYear(0);
		student.setNbSemester(4);
		student.setNbGrant(6);
		assertFalse(crous.additionalGrant(student));
		
		
		student.setProblem(true);
		assertFalse(crous.additionalGrant(student));
		student.setProblem(false);
		student.setStudies(true);
		assertFalse(crous.additionalGrant(student));
		student.setStudies(false);
		student.setIntership(true);
		assertFalse(crous.additionalGrant(student));
	}
	
	@Test
	public void testGiveMoreOrEqualsSeventhRightTrue() {
		
		student.setNbYear(6);
		assertTrue(crous.additionalGrant(student));
		student.setNbYear(7);
		assertTrue(crous.additionalGrant(student));
		
		student.setNbCredit(120);
		student.setNbYear(0);
		student.setNbSemester(0);
		student.setNbGrant(6);
		student.sethOrS(true);
		assertTrue(crous.additionalGrant(student));
		student.setNbGrant(7);
		assertTrue(crous.additionalGrant(student));
	}

}
