package exercices;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatorTU {
	
	private Calculator calculator;
	
	@BeforeEach
	public void setUp() {
		calculator = new Calculator();
	}
	
	@Test
	public void testDivide() {
		assertEquals((float) 2, calculator.divide(4, 2));
	}
	
	@Test
	public void testDivideWithZero() {
		assertEquals(Float.POSITIVE_INFINITY, calculator.divide(8, 0));
	}
	
	@Test
	public void testDivideDecFinished() {
		assertEquals((float) 1.5, calculator.divide(3, 2));
	}
	
	@Test
	public void testDivideDecInfiny() {
		assertEquals(33.333333f, calculator.divide(100, 3));
	}
	
	@Test
	public void testSortAsc() {
		Integer array[] = {8, 77, 15, 14, 46, 13};
		Integer result[] = {8, 13, 14, 15, 46, 77};
		Integer afterFunction [] = calculator.tri(array, "asc");
		for(int i =0; i < array.length; i++) {
			assertEquals(result[i], afterFunction[i]);
		}
	}
	
	@Test
	public void testSortDesc() {
		Integer array[] = {8, 77, 15, 24, 46, 13};
		Integer result[] = {77, 46, 24, 15, 13, 8};
		Integer afterFunction [] = calculator.tri(array, "desc");
		for(int i =0; i < array.length; i++) {
			assertEquals(result[i], afterFunction[i]);
		}
	}
	
	@Test
	public void testSortWith2SameValue() {
		Integer array[] = {8, 77, 15, 24, 46, 15};
		Integer result[] = {77, 46, 24, 15, 15, 8};
		Integer afterFunction [] = calculator.tri(array, "desc");
		for(int i =0; i < array.length; i++) {
			assertEquals(result[i], afterFunction[i]);
		}
	}
	
	@Test
	public void testSortWithNegativeValue() {
		Integer array[] = {8, 77, 15, -24, 46, 13};
		Integer result[] = {-24, 8, 13, 15, 46, 77};
		Integer afterFunction [] = calculator.tri(array, "asc");
		for(int i =0; i < array.length; i++) {
			assertEquals(result[i], afterFunction[i]);
		}
	}
	

}
