package exercices;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ElevatorTU {
	
	private Elevator elevator;
	
	@BeforeEach
	public void setUp() {
		elevator = new Elevator();
	}
	
	@Test
	public void testMovingElevatorSystem() {
		assertEquals(-1, elevator.moveElevator(2, 0));
		assertEquals(0, elevator.moveElevator(0, 0));
		assertEquals(1, elevator.moveElevator(0, 2));
	}
	
	@Test
	public void testStopingElevatorSystem() {
		assertFalse(elevator.stopElevator(10, 0));
		assertTrue(elevator.stopElevator(0, 0));
	}
	
	@Test
	public void testNearestFloor() {
		Integer list[] = {10, 6, 9, -1, 3};
		assertEquals(3, elevator.nearestFloor(4, list));
		assertEquals(3, elevator.nearestFloor(2, list));
		assertEquals(-1, elevator.nearestFloor(0, list));
		assertEquals(6, elevator.nearestFloor(5, list));
		assertEquals(-1, elevator.nearestFloor(-1, list));
		
		Integer listOrder1[] = {10, 6, -1, 3};
		Integer listOrder2[] = {6, 10, -1, 3};
		assertEquals(10, elevator.nearestFloor(8, listOrder1));
		assertEquals(6, elevator.nearestFloor(8, listOrder2));
	}
	
	@Test
	public void testIncorrectValue() {
		Integer list[] = {9, 5, 7, 2, 3, 0};
		Integer listIncorrect[] = {-1, 9, 5, 13, 8, 2};
		assertTrue(elevator.incorrectValue(5, 9, list));
		assertFalse(elevator.incorrectValue(5, 9, listIncorrect));
		assertFalse(elevator.incorrectValue(-2, 9, list));
		assertFalse(elevator.incorrectValue(9, 48, list));
	}
	
	@Test
	public void testMoveElevatorWhitoutRequestFloor() {
		Integer list[] = {6, 2};
		assertEquals(-1, elevator.moveElevatorWRF(3, list));
		assertEquals(0, elevator.moveElevatorWRF(2, list));
		assertEquals(1, elevator.moveElevatorWRF(5, list));
		assertEquals(1, elevator.moveElevatorWRF(4, list));
		
		Integer listOrder[] = {2, 6};
		assertEquals(-1, elevator.moveElevatorWRF(4, listOrder));
	}

}
