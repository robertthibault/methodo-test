package exercices;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class upperTU {
	
	@Test
	public void testMinuscule() {
		String min = "test en minuscule";
		assertEquals("TEST EN MINUSCULE", min.toUpperCase());
	}
	
	@Test
	public void testMajuscule() {
		String maj = "TEST EN MAJUSCULE";
		assertEquals(maj, maj.toUpperCase());
	}
	
	@Test
	public void testMelange() {
		String mel = "Test les 3, m1nuscule, MAJUSCULE et chiFFRE";
		assertEquals("TEST LES 3, M1NUSCULE, MAJUSCULE ET CHIFFRE", mel.toUpperCase());
	}
}
