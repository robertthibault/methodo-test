package exercices;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class LiftTU {

	@Test
    public void test_getFloor_prioritize_null_called_floor(){
        assertEquals(null,new Lift().getFloor(1,null, null));
    }

    @Test
    public void test_getFloor_prioritize_empty_list_pressed(){
        assertEquals(null,new Lift().getFloor(1,null, new Integer[]{}));
    }

    @Test
    public void test_getFloor_prioritize_called_floor(){
        assertEquals(Integer.valueOf(1),new Lift().getFloor(1,1, new Integer[]{2}));
    }


    @Test
    public void test_getFloor_prioritize_closest_floor_above(){
        assertEquals(Integer.valueOf(2),new Lift().getFloor(0,null, new Integer[]{-3,2}));
    }

    @Test
    public void test_getFloor_prioritize_closest_floor_below(){
        assertEquals(Integer.valueOf(1),new Lift().getFloor(3,null, new Integer[]{1,7}));
    }

    @Test
    public void test_getFloor_prioritize_closest_floor_equal_distance_first_in_list_below(){
        assertEquals(Integer.valueOf(1),new Lift().getFloor(1,null, new Integer[]{1,3}));
    }

    @Test
    public void test_getFloor_prioritize_closest_floor_equal_distance_first_in_list_above(){
        assertEquals(Integer.valueOf(1),new Lift().getFloor(1,null, new Integer[]{3,1}));
    }


    @Test
    public void test_getDirection_shoud_stay(){
        assertEquals(0,new Lift().getDirection(1,1, new Integer[]{}));
    }

    @Test
    public void  test_getDirection_should_go_up(){
        assertEquals(1,new Lift().getDirection(0,1, new Integer[]{}));
    }
    @Test
    public void  test_getDirection_should_go_down(){
        assertEquals(-1,new Lift().getDirection(5,1, new Integer[]{}));
    }
}
