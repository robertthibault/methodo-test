package exercices;

public class Crous {

	public Boolean additionalGrant(Student student) {
		Boolean right = false;
		if(student.getId().isBlank() || student.getFirstname().isBlank() || student.getLastname().isBlank()) {
			return right;
		}
		switch (student.getNbGrant()) {
		case 0:
			right = true;
			break;
		case 1:
			right = true;
			break;
		case 2:
			if((student.getNbCredit() >= 60 || student.getNbSemester() >= 2 || student.getNbYear() >= 1)  || (student.getProblem() || student.gethOrS() || student.getStudies() || student.getIntership())) {
				right = true;
			}
			break;
		case 3:
			if((student.getNbCredit() >= 120 || student.getNbSemester() >= 4 || student.getNbYear() >= 2) || ((student.getProblem() || student.gethOrS() || student.getStudies() || student.getIntership()) && !student.getUsed())) {
				right = true;
			}
			break;
		case 4:
			if((student.getNbCredit() >= 120 || student.getNbSemester() >= 4 || student.getNbYear() >= 2) || ((student.getProblem() || student.gethOrS() || student.getStudies() || student.getIntership()) && !student.getUsed())) {
				right = true;
			}
			break;
		case 5:
			if((student.getNbCredit() >= 120 || student.getNbSemester() >= 4 || student.getNbYear() >= 2) && ((student.getProblem() || student.gethOrS() || student.getStudies() || student.getIntership())) && !student.getUsed()) {
				right = true;
			}
			break;
		case 6:
			if((student.getNbCredit() >= 120 || student.getNbSemester() >= 4 || student.getNbYear() >= 2) && student.gethOrS()) {
				right = true;
			}
			break;
		case 7:
			if((student.getNbCredit() >= 120 || student.getNbSemester() >= 4 || student.getNbYear() >= 2) && student.gethOrS() && !student.getUsed()) {
				right = true;
			}
			break;

		default:
			break;
		}
		if((student.getNbYear() >= 4 && student.getNbGrant() < 7) || (student.getNbYear() >= 4 && student.getNbGrant() < 8 && (student.getProblem() || student.getStudies() || student.getIntership()) && !student.getUsed()) || (student.getNbYear() >= 4 && student.getNbGrant() < 10 && student.gethOrS())) {
			right = true;
		}
		return right;
	}
}
