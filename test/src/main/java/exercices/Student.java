package exercices;

public class Student {
	
	private String id;
	private String firstname;
	private String lastname;
	private int nbCredit = 0;
	private int nbSemester = 0;
	private int nbYear = 0;
	private int nbGrant = 0;
	private Boolean problem = false;
	private Boolean studies = false;
	private Boolean hOrS = false;
	private Boolean intership = false;
	private Boolean used = false;
	
//	public Student(String Id, String firstname, String lastname, int nbCredit, int nbSemester, int nbYear) {
//		this.Id = Id;
//		this.firstname = firstname;
//		this.lastname = lastname;
//		this.nbCredit = nbCredit;
//		this.nbSemester = nbSemester;
//		this.nbYear = nbYear;
//	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public int getNbCredit() {
		return nbCredit;
	}

	public void setNbCredit(int nbCredit) {
		this.nbCredit = nbCredit;
	}

	public int getNbSemester() {
		return nbSemester;
	}

	public void setNbSemester(int nbSemester) {
		this.nbSemester = nbSemester;
	}

	public int getNbYear() {
		return nbYear;
	}

	public void setNbYear(int nbYear) {
		this.nbYear = nbYear;
	}

	public int getNbGrant() {
		return nbGrant;
	}

	public void setNbGrant(int nbGrant) {
		this.nbGrant = nbGrant;
	}

	public Boolean getProblem() {
		return problem;
	}

	public void setProblem(Boolean problem) {
		this.problem = problem;
	}

	public Boolean gethOrS() {
		return hOrS;
	}

	public void sethOrS(Boolean hOrS) {
		this.hOrS = hOrS;
	}
	
	public Boolean getStudies() {
		return studies;
	}

	public void setStudies(Boolean studies) {
		this.studies = studies;
	}

	public Boolean getIntership() {
		return intership;
	}

	public void setIntership(Boolean intership) {
		this.intership = intership;
	}

	public Boolean getUsed() {
		return used;
	}

	public void setUsed(Boolean used) {
		this.used = used;
	}
	

}
