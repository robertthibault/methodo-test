package exercices;

public class Lift {
	
	public int getDirection(int currentFloor, Integer askedFloor, Integer calledFloors[]) {
        Integer floorToMoveTo = getFloor(currentFloor,askedFloor,calledFloors);
        if ( floorToMoveTo == null || currentFloor == floorToMoveTo) {
            return 0;
        }else if (floorToMoveTo > currentFloor){
            return 1;
        }
        return -1;
    }

    public Integer getFloor(int currentFloor, Integer askedFloor, Integer calledFloors[]){
        if (askedFloor != null) {
            return askedFloor;
        } else if (calledFloors != null && calledFloors.length > 0) {
            Integer delta = null;
            Integer result = null;

            for( Integer floor : calledFloors){
                int currentDelta = Math.abs(currentFloor - floor);
                if( delta == null || delta > currentDelta ){
                    delta = currentDelta;
                    result = floor;
                }
            }
            return result;
        }
        return null;
    }

    public boolean shouldStop(int currentFloor, Integer askedFloor, Integer calledFloors[] ){
        return false;
    }

}
