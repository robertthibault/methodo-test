package exercices;

public class Elevator {
	
	private int minFloor = -1;
	private int maxFloor = 10;
	
	public int moveElevator(int currentFloor, int requestedFloor) {
		int resultDisplacement = 0;
		if(currentFloor > requestedFloor) {
			resultDisplacement = -1;
		} else if (currentFloor < requestedFloor){
			resultDisplacement = 1;
		}
		return resultDisplacement;
	}
	
	public Boolean stopElevator(int currentFloor, int requestedFloor) {
		return currentFloor == requestedFloor;
	}
	
	public int nearestFloor(int currentFloor, Integer list[]) {
		if(list.length != 0 || list.length != -1) {
			int result = 0;
			int counter = 100;
			for (Integer value : list) {
				int calculation = Math.abs(currentFloor - value);
				if(calculation < counter) {
					counter = calculation;
					result = value;
				}
			}
			return result;
		} else {
			return -1;
		}
	}
	
	public Boolean incorrectValue(int currentFloor, int requestedFloor, Integer list[]) {
		Boolean test = currentFloor < minFloor || currentFloor > maxFloor || requestedFloor < minFloor || requestedFloor > maxFloor;
		if(!test) {
			for (Integer value : list) {
				if(value < minFloor || value > maxFloor) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	public int moveElevatorWRF(int currentFloor, Integer list[]) {
		int requestedFloor = nearestFloor(currentFloor, list);
		int resultDisplacement = 0;
		if(currentFloor > requestedFloor) {
			resultDisplacement = -1;
		} else if (currentFloor < requestedFloor){
			resultDisplacement = 1;
		}
		return resultDisplacement;
	}

}
