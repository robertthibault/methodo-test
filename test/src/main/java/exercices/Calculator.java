package exercices;

import java.util.Arrays;
import java.util.Collections;

public class Calculator {

	public Float divide(int a, int b) {
		return (float) a/b;
	}
	
	public Integer[] tri(Integer list[], String order) {
		if(order == "asc") {
			Arrays.sort(list);
			return list;
		} else if(order == "desc") {
			Arrays.sort(list, Collections.reverseOrder());
			return list;
		}
		return list;
	}
}
